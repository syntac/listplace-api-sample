<?php
	$url 	= 'http://listplace.info/api/?user=register';
	$ch 	= curl_init();
	$data 	= array(
		"username" 	=> "username",
		"password" 	=> "p4ssw0rd$",
		"email" 	=> "willy@listplace.dev",
		"firstname"	=> "Willy",
		"lastname" 	=> "Arisky",
		"address" 	=> "Jl. Langkolo no.21",
		"addressLine2" => "Ds. Neglasari",
		"city" 		=> "Sukabumi",
		"zipcode" 	=> "43356",
		"state" 	=> "Jawa Barat",
		"country" 	=> "ID",
	);

	$options = array(
	    CURLOPT_URL				=> $url, // Set URL of API
	    CURLOPT_CUSTOMREQUEST 	=> "POST", // Set request method
	    CURLOPT_RETURNTRANSFER	=> true, // true, to return the transfer as a string
	    CURLOPT_POSTFIELDS 		=> json_encode($data), // Send the data in HTTP POST Note: api only accept json data
	);
	curl_setopt_array( $ch, $options );
	// Execute and Get the response
	$response = curl_exec($ch);
	// Get HTTP Code response
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	// Close cURL session
	curl_close($ch);
	// Show response
	echo '<h3>HTTP Code</h3>';
	echo $httpCode;
	echo '<h3>Response</h3>';
	echo $response;
?>
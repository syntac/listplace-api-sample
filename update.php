<form method="post">
Business ID:<input type="text" name="business_id" required> <br>
Business Name:<input type="text" name="business_name" required> <br>
<button type="submit" name="update">Update Business</button>
</form>

<?php
if( isset($_POST['update']) ){
	// example input data from text field
	$business_name = $_POST['business_name'];
	$business_id = $_POST['business_id'];

	$url 	= 'http://listplace.dev/api/?business=update&key=f843ac31893b4e1af48108e8048b8b75e7e33d58';
	$ch 	= curl_init();
	$data 	= array(
		"business_id" => $business_id,
		"business_name" => $business_name,
		"email" => "willy@syntac.co.id",
		"business_website" => "syntac.co.id",
		"business_phone_number" => "+628981848440",
		"category" => "Online Store",
		"description" => "Description text here",
		"onlineonly" => "yes",
		"keywords" => "software development, ui designer",
		"contactPersonPosition" => "Web Developer",
		"contactPerson" => "Willy Arisky",
		"country" => "ID",
		"state" => "", 
		"city" => "Sukabumi",
		"addressLine1" => "Jl. Raya Ciambar",
		"addressLine2" => "Kp. Neglasari",
		"zipCode" => "43357",
		"geo_lat" => "-6.833600833543404",
		"geo_lon" => "106.77733245291745",
		"businessHours" => array(
			"day" 	=> array("mo", "tue", "we"),
			"open"	=> array("08:00","08:00", "08:00"),
			"close"	=> array("17:00","17:00", "17:00"),
		),
		"images" => array("url"=>"https://assets.entrepreneur.com/content/16x9/822/20160118164234-interior-modern-office-desks-space-computers.jpeg"),
	);

	$options = array(
	    CURLOPT_URL				=> $url, // Set URL of API
	    CURLOPT_CUSTOMREQUEST 	=> "POST", // Set request method
	    CURLOPT_RETURNTRANSFER	=> true, // true, to return the transfer as a string
	    CURLOPT_POSTFIELDS 		=> json_encode($data), // Send the data in HTTP POST Note: api only accept json data
	);
	curl_setopt_array( $ch, $options );
	// Execute and Get the response
	$response = curl_exec($ch);
	// Get HTTP Code response
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	// Close cURL session
	curl_close($ch);
	// Show response
	echo '<h3>HTTP Code</h3>';
	echo $httpCode;
	echo '<h3>Response</h3>';
	echo $response;
}
?>
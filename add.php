<form method="post">
Business Name:<input type="text" name="business_name" required> <br>
<button type="submit" name="add">Add Business</button>
</form>

<?php
if( isset($_POST['add']) ){
	// example input data from text field
	$business_name = $_POST['business_name'];

	$url 	= 'http://listplace.dev/api/?business=new&key=f843ac31893b4e1af48108e8048b8b75e7e33d58';
	$ch 	= curl_init();
	$data 	= array(
		"business_name" => $business_name,
		"email" => "willy@syntac.co.id",
		"business_website" => "syntac.co.id",
		"business_phone_number" => "+628981848440",
		"category" => "Software Development",
		"description" => "Description text here",
		"onlineonly" => "yes",
		"keywords" => "web designer, software development, ui designer",
		"contactPersonPosition" => "Web Developer",
		"contactPerson" => "Willy Arisky",
		"country" => "ID",
		"state" => "", 
		"city" => "Sukabumi",
		"addressLine1" => "Jl. Langkolo No.15",
		"addressLine2" => "Kp. Neglasari, Kec. Ciambar",
		"zipCode" => "43357",
		"geo_lat" => "-6.833600833543404",
		"geo_lon" => "106.77733245291745",
		"businessHours" => array(
			"day" 	=> array("mo", "tue", "we"),
			"open"	=> array("08:00","08:00", "08:00"),
			"close"	=> array("17:00","17:00", "17:00"),
		),
		"images" => Array( "0" => "http://cdn.sitebooster.co/i/58fa752b839d156db4039069/3ae7a5f9d9b14502ad5456eb4015c744.jpg", "1" => "http://cdn.sitebooster.co/i/58fa752b839d156db4039069/458b2cb6244c414f9da8cb5174a3599e.jpg" ),
	);

	$options = array(
	    CURLOPT_URL				=> $url, // Set URL of API
	    CURLOPT_CUSTOMREQUEST 	=> "POST", // Set request method
	    CURLOPT_RETURNTRANSFER	=> true, // true, to return the transfer as a string
	    CURLOPT_POSTFIELDS 		=> json_encode($data), // Send the data in HTTP POST Note: api only accept json data
	);
	curl_setopt_array( $ch, $options );
	// Execute and Get the response
	$response = curl_exec($ch);
	// Get HTTP Code response
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	// Close cURL session
	curl_close($ch);
	// Show response
	echo '<h3>HTTP Code</h3>';
	echo $httpCode;
	echo '<h3>Response</h3>';
	echo $response;
}
?>
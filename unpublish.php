<form method="post">
Business ID:<input type="text" name="business_id" required> <br>
<button type="submit" name="unpublish">Unpublish Business</button>
</form>

<?php
if( isset($_POST['unpublish']) ){
	// example input data from text field
	$business_id = $_POST['business_id'];

	$url 	= 'http://listplace.dev/api/?business=unpublish&key=f843ac31893b4e1af48108e8048b8b75e7e33d58';
	$ch 	= curl_init();

	$data 	= array( "business_id" => $business_id );

	$options = array(
	    CURLOPT_URL				=> $url, // Set URL of API
	    CURLOPT_CUSTOMREQUEST 	=> "POST", // Set request method
	    CURLOPT_RETURNTRANSFER	=> true, // true, to return the transfer as a string
	    CURLOPT_POSTFIELDS 		=> json_encode($data), // Send the data in HTTP POST Note: api only accept json data
	);

	curl_setopt_array( $ch, $options );
	// Execute and Get the response
	$response = curl_exec($ch);
	// Get HTTP Code response
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	// Close cURL session
	curl_close($ch);
	// Show response
	echo '<h3>HTTP Code</h3>';
	echo $httpCode;
	echo '<h3>Response</h3>';
	echo $response;
}
?>